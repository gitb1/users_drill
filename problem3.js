function sortUsersBySeniority(users) {
    const designationOrder = {
        "Senior": 3,
        "Developer": 2,
        "Intern": 1
    };

    return Object.keys(users).sort((user1, user2) => {
        const extractKeyword = (designation) => {
            if (typeof designation !== 'string') return '';
            const keyword = designation.split(' ')[0]; // Extract the first word
            return keyword in designationOrder ? keyword : ''; // Return the keyword if it's in the order, otherwise ''
        };

        const user1Keyword = extractKeyword(users[user1].designation);
        const user2Keyword = extractKeyword(users[user2].designation);
        const user1Order = designationOrder[user1Keyword] || 0; // Use 0 if keyword not found
        const user2Order = designationOrder[user2Keyword] || 0; // Use 0 if keyword not found

        // Sort by designation order first
        if (user1Order !== user2Order) {
            return user2Order - user1Order;
        }

        // If designation order is the same, order by age (descending)
        return users[user2].age - users[user1].age;
    });
}


module.exports=sortUsersBySeniority;