
function findVideoGamePlayers(users) {
  try{
    let videoGamePlayers = [];
    for (const user in users) {
        let arr = users[user].interests;
        for(let key of arr){
            if ( key.includes("Video Games")) {
                videoGamePlayers.push(user);
            }
        }
    
    }
    return videoGamePlayers;
    }catch(error){
        console.log('Error is :',error.message);
    }
}

module.exports= findVideoGamePlayers;