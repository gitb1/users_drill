
function groupUsersByProgrammingLanguage(users) {
   try{
    let groupedUsers = {};
    for (const user in users) {
        const designation = users[user].desgination.toLowerCase();
        const language = designation.includes("golang") ? "Golang" :
                         designation.includes("javascript") ? "Javascript" :
                         designation.includes("python") ? "Python" : "Other";
        if (!groupedUsers[language]) {
            groupedUsers[language] = [];
        }
        groupedUsers[language].push(user);
    }
    return groupedUsers;
   }catch(error){
    console.log('Error is :',error.message);  
   }
}

module.exports=groupUsersByProgrammingLanguage;