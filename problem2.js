function findGerman(users) {
  try {
    let results = [];
    for (const user in users){
      if (users[user].nationality === "Germany") {
        results.push(user);
      }
    }
    return results;
  } catch (error) {
    console.log("Error is :", error.message);
  }
}

module.exports=findGerman;
